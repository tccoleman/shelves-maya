"""
    Creates deform_utils Maya shelf

    To use, run these Python commands in Maya:
        import sys
        sys.path.append("D:/Users/Tcoleman/Documents/dev/shelves-maya")

        from shelves_maya import shelf_rigAnimUtils
        reload(shelf_rigAnimUtils)
        shelf_rigAnimUtils.load(name="RigAnim_utils")

"""
import os
SHELF_LOCATION = os.path.realpath(os.path.dirname(__file__))
ICON_DIR = os.path.join(os.path.dirname(__file__), 'shelf_rigAnimUtils_icons')
SCRIPTS_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), 'shelf_rigAnimUtils_scripts'))

VERSION_MAJOR = 0
VERSION_MINOR = 1
VERSION_PATCH = 0

version_info = (VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH)
version = '{}.{}.{}'.format(*version_info)
__version__ = version

__all__ = ['version', 'version_info', '__version__']

import logging
LOG = logging.getLogger(__name__)

import sys
import os
import ntpath
import subprocess

from shelves_maya import shelf_base

import maya.cmds as cmds
import maya.mel as mel


#=============================
# CUSTOM SHELF COMMAND FUNCTIONS
#=============================

def launch_face_picker():

    PICKER_DIR = SCRIPTS_DIR.replace('\\', '/')
    mel.eval('source "{}/RigManager/Rig_Manager_init.mel"'.format(PICKER_DIR))
    mel.eval("Rig_Manager_init();")
    mel.eval('source "{}/RigManager/Rig_Manager_v3.2.mel"'.format(PICKER_DIR))
    #mel.eval('run_RigManager("{}/RigManager/");'.format(PICKER_DIR))
    #mel.eval("Update_Characters_List();")


def exploreMayaProject():
    projDir = cmds.workspace(rd=True, q=True)
    subprocess.Popen(r'explorer /select,"%sscenes"' % projDir.replace("/", "\\"))
    LOG.info('Exploring to %s' % projDir)


def launch_fbxImportToNamespace():

    sys.path.append(SCRIPTS_DIR)
    import fbxNamespaceImporter.ui
    reload(fbxNamespaceImporter.ui)
    fbxNamespaceImporter.ui.show()


def reload_shelf():
    """Reloads Dragonfly shelf"""
    try:
        from shelves_maya import shelf_rigAnimUtils
        reload(shelf_rigAnimUtils)
        shelf_rigAnimUtils.load(name="RigAnim_utils")
        return True
    except:
        LOG.error("Error reloading shelf")
        return


def remapTexturePaths():
    fileNodes = cmds.ls(type='file')
    if fileNodes:
        for fileTx in fileNodes:
            try:
                curPath = cmds.getAttr("{}.fileTextureName".format(fileTx))
                if "Textures" in curPath:
                    newPath = "../../../Textures" + curPath.split('Textures')[1]
                    cmds.setAttr("{}.fileTextureName".format(fileTx), newPath, type="string")
                    LOG.info("Successfully remapped texture path for: {}".format(fileTx))
            except:
                LOG.warning("Unable to remap texture path for: {}".format(fileTx))
                pass

def launch_picker():

    PICKER_DIR = SCRIPTS_DIR.replace('\\', '/')
    mel.eval('source "{}//picker.mel"'.format(PICKER_DIR))

def launch_biped_picker():

    PICKER_DIR = SCRIPTS_DIR.replace('\\', '/')
    mel.eval('source "{}//biped.mel"'.format(PICKER_DIR))


def launch_anim_import_export(mode="import"):
    ANIMIO_DIR = SCRIPTS_DIR.replace('\\', '/')
    mel.eval('source "{}//animImportExport.mel"'.format(ANIMIO_DIR))

    if "import" in mode:
        mel.eval('animFileLoad();')

    elif "export" in mode:
        mel.eval('animFileSave();')

    else:
        pass


def launch_face_shader_export():
    sys.path.append(SCRIPTS_DIR)
    import face_shader_export
    reload(face_shader_export)
    face_shader_export.export_baked_face_shader()


def launch_gameExporter():
    presetNode = ""
    geNodes = cmds.ls(type="gameFbxExporter")

    if geNodes:
        presetNode = geNodes[-1]

    else:
        presetNode = cmds.createNode("gameFbxExporter")

    # FBX Export settings
    cmds.setAttr("{}.presetName".format(presetNode), "anim_export", type="string")

    cmds.setAttr("{}.exportPath".format(presetNode), "fbx", type="string")
    cmds.getAttr("{}.exportPath".format(presetNode))

    cmds.setAttr("{}.exportTypeIndex".format(presetNode), 2)
    cmds.setAttr("{}.exportSetIndex".format(presetNode), 2)
    cmds.setAttr("{}.fileSplitType".format(presetNode), 1)

    cmds.setAttr("{}.modelFileMode".format(presetNode), 0)

    cmds.setAttr("{}.fileVersion".format(presetNode), "FBX201400", type="string")

    cmds.setAttr("{}.fileSplitType".format(presetNode), 2)

    cmds.setAttr("{}.upAxis".format(presetNode), 1)
    cmds.setAttr("{}.embedMedia".format(presetNode), 0)
    cmds.setAttr("{}.inputConnections".format(presetNode), 0)
    cmds.setAttr("{}.autoScaleFactor".format(presetNode), 0)
    cmds.setAttr("{}.showWarningManager".format(presetNode), 0)
    cmds.setAttr("{}.generateLogData".format(presetNode), 0)
    cmds.setAttr("{}.fileType".format(presetNode), 0)

    cmds.setAttr("{}.isTheLastOneSelected".format(presetNode), 1)
    cmds.setAttr("{}.isTheLastOneUsed".format(presetNode), 1)

    cmds.setAttr("{}.animClips[0].animClipStart".format(presetNode), cmds.playbackOptions(q=True, min=True))
    cmds.setAttr("{}.animClips[0].animClipEnd".format(presetNode), cmds.playbackOptions(q=True, max=True))
    cmds.setAttr("{}.animClips[0].exportAnimClip".format(presetNode), 1)

    mel.eval("gameFbxExporter;")


def alembic_export_selected():
    mel.eval("AlembicExportSelectionOptions;")


def isReference(refNode):
    """Query if the given node is a valid reference node

    Args:
        refNode: Reference node to query
    """
    # Check reference node
    if refNode not in listReferences(): return False

    return True

def isReferenced(node):
    """Query if the given node is referenced from an external file

    Args:
        node: Reference node to query
    """
    # Check referenced node
    if cmds.referenceQuery(node, isNodeReferenced=True): return True
    else: return False

def getReferenceNode(node):
    """Get the reference node associated with the given referenced object

    Args:
        refNode: Reference node to query
    """
    # Check Referenced Node
    if not isReferenced(node):
        raise Exception('Object "' + node + '" is not a referenced node!')

    # Get Reference Node
    refNode = cmds.referenceQuery(node, referenceNode=True)

    return refNode

def getReferenceFile(node, withoutCopyNumber=True):
    """Get the reference file associated with the given referenced object or reference node

    Args:
        refNode: Reference node to query
    """
    # Check Reference/Referenced Node
    if not isReference(node) and not isReferenced(node):
        raise Exception('Object "' + node + '" is not a valid reference node or a node from a referenced file!')

    # Get Reference details
    refFile = cmds.referenceQuery(node, filename=True, wcn=withoutCopyNumber)

    return refFile

def listReferences(parentNS=None):
    """Return a list of reference nodes found in the current scene
    @param
    Args:
        parentNS: Parent namespace to query reference nodes from
    """
    # Get Reference List
    refNodes = []
    for ref in cmds.ls(type='reference'):
        if 'sharedReferenceNode' in ref: continue
        if '_UNKNOWN_REF_NODE_' in ref: continue
        if parentNS:
            if not ref.startswith(parentNS):
                continue
        refNodes.append(ref)

    return refNodes

def replaceReference(refNode, refPath, verbose=True):
    """Replace the reference file path for a specified reference node.

    Args:
        refNode: Reference node to replace file path for
        refPath: New reference file path
    """
    # Check reference node
    if not isReference(refNode):
        raise Exception('Object "' + refNode + '" is not a valid reference node!')

    # Check reference file
    if getReferenceFile(refNode, withoutCopyNumber=True) == refPath:
        print ('Reference "' + refNode + '" already referencing "' + refPath + '"!')
        return

    # Get file type
    refType = ''
    if refPath.endswith('.ma'): refType = 'mayaAscii'
    elif refPath.endswith('.mb'): refType = 'mayaBinary'
    else: raise Exception('Invalid file type! ("' + refPath + '")')

    # Replace reference
    cmds.file(refPath, loadReference=refNode, typ=refType, options='v=0')

    # Print message
    if verbose: print('Replaced reference "' + refNode + '" using file - "' + refPath + '"')

    return refPath


def unloadReference(refNode, verbose=True):
    """Unload the reference associated with the given reference node

    Args:
        refNode: Reference node to unload
    """
    # Check reference node
    if not isReference(refNode):
        raise Exception('Object "' + refNode + '" is not a valid reference node!')

    # Unload Reference
    cmds.file(referenceNode=refNode, unloadReference=True)

    # Print message
    if verbose: print('Unloadeded reference "' + refNode + '"! (' + getReferenceFile(refNode) + ')')


def reloadReference(refNode, verbose=True):
    """Reload the reference associated with the given reference node

    Args:
        refNode: Reference node to reload
    """
    # Check reference node
    if not isReference(refNode):
        raise Exception('Object "' + refNode + '" is not a valid reference node!')

    # Unload Reference
    cmds.file(referenceNode=refNode, loadReference=True)

    # Print message
    if verbose: print('Reloadeded reference "' + refNode + '"! (' + getReferenceFile(refNode) + ')')


def swapReference(res="hi"):
    """Swaps referenced resolution rig by filename of other resolution rigs in same directory.
    Node from referenced rig needs to be selected before running.

    You can also set res to "off/on" to unload/reload the reference file as well.  When reloading, and
    there's no rig in the scene, select the reference node(s) for the objects before reloading.

    Requires rig file naming convention:
        <assetName>_rig_<res>
        ostritch_rig_hi.ma
    """
    sel = cmds.ls(selection=True)

    if sel:
        sel_off = list()
        for node in sel:
            if "reference" in cmds.nodeType(node) and "on" in res:
                reloadReference(node)
            else:
                rfn = ""
                if "reference" in cmds.nodeType(node):
                    rfn = node
                else:
                    rfn = getReferenceNode(node)
                rf = getReferenceFile(node, withoutCopyNumber=True)

                if rf:
                    path = os.path.dirname(rf)
                    current_ref_file = ntpath.basename(rf)
                    current_ext = current_ref_file.split('.')
                    current_res = current_ext[0].split("_")[-1]
                    res_ref_file = current_ref_file.replace(current_res, res)
                    new_path = rf.replace(current_ref_file, res_ref_file)

                    if not "off" in res:
                        if os.path.isfile(new_path):
                            replaceReference(rfn, new_path)
                        else:
                            LOG.warning("{} file does not exist!".format(new_path))
                    else:
                        unloadReference(rfn)
                        sel_off.append(rfn)
                else:
                    LOG.warning("No reference file associated with {}, skipping".format(node))
        try:
            cmds.select(sel)
        except:
            print sel_off
            cmds.select(sel_off)
        finally:
            pass

def setViewportTextureSizeClamp(textureSize=1024):
    cmds.setAttr("hardwareRenderingGlobals.enableTextureMaxRes", 1)
    cmds.setAttr("hardwareRenderingGlobals.textureMaxResolution", textureSize)


#=============================
# CUSTOM SHELF FUNCTION
#=============================
class load(shelf_base._shelf):

    def build(self):

        # PROJECT
        self.addButon(label="", icon=ICON_DIR + "/mayaProjectTools.png")
        mayaProj_menu = cmds.popupMenu(b=1)
        self.addMenuItem(mayaProj_menu, "Set Maya Project",
                         command="import maya.mel as mel; mel.eval('SetProject')")
        self.addMenuItem(mayaProj_menu, "Explore Maya Project",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.exploreMayaProject()")
        self.addMenuItem(mayaProj_menu, "Reload this shelf",
                         command="from shelves_maya import shelf_rigAnimUtils; maya.utils.executeDeferred('shelf_rigAnimUtils.reload_shelf()')")

        self.addButon(label="", icon=ICON_DIR + "/sep.png")

        # Animation Import Export
        self.addButon(label="", icon=ICON_DIR + "/anim.png")
        aru_shelf_anim_menu = cmds.popupMenu(b=1)

        self.addMenuItem(aru_shelf_anim_menu, "FBX Import Anim to Namespace",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_fbxImportToNamespace()")

        self.addMenuItem(aru_shelf_anim_menu, "FBX Animation Exporter",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_gameExporter()")

        self.addMenuItem(aru_shelf_anim_menu, "Animation Export", command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_anim_import_export(mode='export')")
        self.addMenuItem(aru_shelf_anim_menu, "Animation Import", command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_anim_import_export(mode='import')")


        # PICKERS
        self.addButon(label="", icon=ICON_DIR + "/pickers.png")
        aru_shelf_pickers_menu = cmds.popupMenu(b=1)

        #self.addMenuItem(aru_shelf_pickers_menu, "Open Picker",
        #                 command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_picker()")

        self.addMenuItem(aru_shelf_pickers_menu, "Open Body Picker",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_biped_picker()")

        self.addMenuItem(aru_shelf_pickers_menu, "Open Face Picker",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_face_picker()")

        #self.addMenuItem(aru_shelf_pickers_menu, "Update Face Picker Character List",
        #                 command='import maya.mel as mel; mel.eval("Update_Characters_List();")')



        # TEXTURES
        self.addButon(label="", icon=ICON_DIR + "/textures.png")
        aru_shelf_textures_menu = cmds.popupMenu(b=1)

        self.addMenuItem(aru_shelf_textures_menu, "Bake and export face wrinkle shader animation...",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.launch_face_shader_export()")
        self.addMenuItem(aru_shelf_textures_menu, "Set VP 2.0 Max Texture Size",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.setViewportTextureSizeClamp(textureSize=1024)")
        self.addMenuItem(aru_shelf_textures_menu, "File Path Editor",
                         command='import maya.mel as mel; mel.eval("FilePathEditor;")')
        #self.addMenuItem(aru_shelf_textures_menu, "Remap texture paths",
        #                 command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.remapTexturePaths()")
        #self.addMenuItem(aru_shelf_textures_menu, "Set VP 2.0 Depth Peeling",
        #                 command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.setVP2_depthPeeling()")

        
        # CACHING
        self.addButon(label="", icon=ICON_DIR + "/cache.png")
        aru_shelf_cache_menu = cmds.popupMenu(b=1)

        self.addMenuItem(aru_shelf_cache_menu, "Alembic Cache Selected",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.alembic_export_selected()")

        # TOOLS
        """
        self.addButon(label="", icon=ICON_DIR + "/tools.png")
        aru_shelf_tools_menu = cmds.popupMenu(b=1)

        self.addMenuItem(aru_shelf_tools_menu, "Swap selected Referenced Rigs to HIRES",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.swapReference(res='hi')")
        self.addMenuItem(aru_shelf_tools_menu, "Swap selected Referenced Rigs to MIDRES",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.swapReference(res='mid')")
        self.addMenuItem(aru_shelf_tools_menu, "Swap selected Referenced Rigs to LORES",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.swapReference(res='lo')")
        self.addMenuItem(aru_shelf_tools_menu, "UNLOAD selected Referenced Rigs",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.swapReference(res='off')")
        self.addMenuItem(aru_shelf_tools_menu, "RELOAD selected Referenced Rigs (select reference nodes first!)",
                         command="from shelves_maya import shelf_rigAnimUtils; shelf_rigAnimUtils.swapReference(res='on')")
        """
        LOG.info("Loaded shelf: %s" % self.name)
