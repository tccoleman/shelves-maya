//------------------------------------------------------------------------------------------- 
// rig_tools_AnimImportExport.mel #
//
//
// Description: These scripts import and export animation
//
//		animFileSave - use this to save out the animation on whatever is selected
//		animFileLoad - use this to load the animation saved with rig_SelAnimFileSave
//
// Notes:	o the animation is saved out into a Maya ascii file (default) or mayaBinary
//   		o entire animation curves are saved - no start/end times supported
//			o the scripts only export animation channels on selected objects
//			o the scripts support prefix renaming, attribute renaming and pasting to locked channels
//
//	v002 - Added code to "rig_MultiPasteAnim" to handle human animation files.
//
//------------------------------------------------------------------------------------------- 

global proc animFileSave() {

	string $selected[] = `ls -sl`;

	if (size($selected) == 0) {
		error ("animFileSave: Nothing selected.");
	}
	//fileBrowser("saveAnimCallBack","Save","mayaAscii",1);
	string $basicFilter = "Maya Files (*.ma *.mb)";
    string $result[] = `fileDialog2 -fileFilter $basicFilter -dialogStyle 2`;
    saveAnimCallBack($result[0], "mayaAscii") ;

} // animFileSave 


global proc animFileLoad() {

	global string $rig_SelObjList[];

	$rig_SelObjList = `ls -sl`;

	//fileBrowser("assignrig_loadSelCallBack","Load","mayaAscii",0);
	string $basicFilter = "Maya Files (*.ma *.mb)";
    string $result[] = `fileDialog2 -fileFilter $basicFilter -dialogStyle 2 -fm 1`;
    loadAnimCallBack($result[0], "mayaAscii") ;

} // animFileLoad 


// This gets run when you press the 'save' button
global proc int saveAnimCallBack(string $fileName, string $fileType) {

	string $fname;
	string $objList[] = `ls -sl`;
	int    $numKeys;
	string $attrList[];
	string $filePath[];

	if ( gmatch ($fileName, "*.ma")  || gmatch ($fileName,"*.mb") ) {

		$fname = `substring $fileName 1 (size($fileName) - 3)`;

	} else {

		$fname = $fileName;
	}

	tokenize($fname, "/", $filePath);
	string $buf = $filePath[size($filePath)-1];
	string $fileObj = substituteAllString($buf, "_", "");

	print ("fileName = " + $fileName + "\n");
	print ("fileType = " + $fileType + "\n");
	print ("fname = " + $fname + "\n");
	print ("fileObj = " + $fileObj + "\n");

	// Loop Through Object List
	string $TEMP_NODE = `createNode "transform" -n ("__ANIM_TEMP_" + $fileObj)`;
        
	for ($objCur in $objList) {

		if (`objExists $objCur`) {
		    string $ns;
		    string $objName = $objCur;
		    if (`gmatch $objCur "*:*"`){
                // Get namespace
                string $tokenized[];
                tokenize $objCur ":" $tokenized;
                $ns = $tokenized[0];
                $objName = $tokenized[1];
            }

			$attrList = `listAttr -k $objCur`;

			for ($attrCur in $attrList) {

				$numKeys = `keyframe -t ":" -query -keyframeCount ($objCur + "." + $attrCur)`;
				print( "Number of keys existing: " + $numKeys + " for object " + $objCur + "." + $attrCur + "\n" );

				if ($numKeys > 0) {
				    if (gmatch("", $ns) != "") {
					    addAttr -ln ($ns + "__" + $objName + "ANIM" + $attrCur) -at double $TEMP_NODE;
					}
					else{
					    addAttr -ln ($objName + "ANIM" + $attrCur) -at double $TEMP_NODE;
					}
					$numKeys = `copyKey -t ":"  -at $attrCur $objCur`;
					print( "Number of keys copied: " + $numKeys + "\n");

					if (gmatch("", $ns) != "") {
					    $numKeys = `pasteKey  -option "replaceCompletely" -at ($ns + "__" + $objName + "ANIM" + $attrCur) $TEMP_NODE`;
					}
					else{
					    $numKeys = `pasteKey  -option "replaceCompletely" -at ($objCur + "ANIM" + $attrCur) $TEMP_NODE`;
					}
					//$numKeys = `pasteKey  -option "replaceCompletely" -at ($objCur + "ANIM" + $attrCur) $TEMP_NODE`;
					print( "Number of keys pasted: " + $numKeys + "\n");
				}
                        
			}
		}
	}

	// Export StoreHouse
	//select ("TEMP" + $fileObj);
	select $TEMP_NODE ;

	print ("Saving animation to file: " + $fileName + "\n");

	if ( catch(`file -es -channels true -type $fileType $fileName`) ) warning ("rig_SaveAnim: Error saving file.");
	
	//delete ("TEMP" + $fileObj);
	delete $TEMP_NODE ;

	return true;

} // animSaveCallBack


// This gets run when you press the 'load' button
global proc int loadAnimCallBack(string $fname, string $fileType) {

	string $filename;
	string $attrList[];
	string $nameBuf[];
	string $filePath[];
	int $numTokens;
	string $prefix = "";
	string $tokens[];

	if ( gmatch ($fname, "*.ma")  || gmatch ($fname,"*.mb") ) {

		$filename = `substring $fname 1 (size($fname) - 3)`;

	} else {

		$filename = $fname;
	}

	// import the file here 
	file -i $fname  ;

	// get our list of attributes
	tokenize($filename, "/", $filePath);
	string $buf = $filePath[size($filePath)-1];
	string $fileObj = substituteAllString($buf, "_", "");
	string $TEMP_NODE = "__ANIM_TEMP_" + $fileObj;
	$attrList = `listAttr $TEMP_NODE`;

	// loop through the attributes until we find a saved
	// animation channel then extract the object name
	for ($attrCur in $attrList) {

		if (match("ANIM", $attrCur) != "") {       

			string $tempString = substitute ("ANIM", $attrCur, "^");
			tokenize( $tempString, "^", $nameBuf); 
			break;
		}
	}

	// extract the prefix
	$numTokens = `tokenize $nameBuf[0] "_" $tokens`;

	loadAnimGUI($filename, $prefix);

	return true;

} // animLoadCallBack


//------------------------------------------------------------------------------------------- 
// loadAnimGUI
//
// Description: 
//
// Inputs: string $filename - the filename (this is just passed to rig_PasteSelAnim) 
//         string $prefix   - the prefix to be put in the 'old prefix' field
//
//
//
//------------------------------------------------------------------------------------------- 

global proc loadAnimGUI (string $filename, string $prefix) { 

	int $uiWidth  = 380;
	int $uiHeight = 200;
	string $prfx  = "";

	if (size($prefix) > 0) $prfx = `substring $prefix 1 (size($prefix) - 1)`;

	if (( `window -ex rig_LoadSelAnimWindow`) == true) deleteUI rig_LoadSelAnimWindow;
	window	-title "rig_ Load Animation"
			-w $uiWidth -h $uiHeight
			-rtf true
			rig_LoadSelAnimWindow;

	string $form = `formLayout -numberOfDivisions 2`;

	string $frame =`frameLayout -labelVisible false -mh 5 -mw 5`;

	string $col = `columnLayout -adjustableColumn true -parent $frame`;

	// prefix
	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
	text -l "Replace prefix";
	checkBox	-h 20	-value 0 -label ""
					-onc "textField -e -ed 1 rig_TextOldPre; textField -e -ed 1 rig_TextNewPre;"
					-ofc "textField -e -ed 0 rig_TextOldPre; textField -e -ed 0 rig_TextNewPre;"
					cbPrefixReplace;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "Old prefix";
		textField -w 200 -text $prefix -ed 0 rig_TextOldPre;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "New prefix";
		textField -w 200 -ed 0 rig_TextNewPre;

	setParent ..;
	separator -height 10;

	// search and replace
	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
	text -l "Search & Replace";
	checkBox	-h 20	-value 0 -label ""
					-onc "textField -e -ed 1 rig_TextOld; textField -e -ed 1 rig_TextNew;"
					-ofc "textField -e -ed 0 rig_TextOld; textField -e -ed 0 rig_TextNew;"
					cbSearchReplace;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "Replace: ";
		textField -w 200 -ed 0 rig_TextOld;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "With: ";
		textField -w 200 -ed 0 rig_TextNew;

	setParent ..;
	separator -height 10;


	// paste method 
	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "Paste Method";
		radioButtonGrp -numberOfRadioButtons 3
					-labelArray3 "Insert" "Replace" "Replace Completely"
					-cw 1 50 -cw 2 70 -cw 3 120 -select 3
					-on1 "intField -e -ed 1 rig_StartFrameIF; intField -e -ed 0 rig_EndFrameIF; intField -e -ed 0 rig_OffsetIF;" 
					-on2 "intField -e -ed 1 rig_StartFrameIF; intField -e -ed 1 rig_EndFrameIF; intField -e -ed 1 rig_OffsetIF;"
					-on3 "intField -e -ed 0 rig_StartFrameIF; intField -e -ed 0 rig_EndFrameIF; intField -ed 0 rig_OffsetIF;"
					rig_PasteMethBG;

	setParent ..;
	separator -style "none" -height 10;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "Start Frame";
		intField  -v 0 -ed 0 rig_StartFrameIF;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "End Frame";
		intField -v 0 -ed 0 rig_EndFrameIF;

	setParent ..;
	rowLayout	-numberOfColumns 2 -cat 1 "right" 5 -columnWidth 1 95 -parent $col;
		text -l "Offset";
		intField -v 0 -ed 0 rig_OffsetIF;

	setParent ..;
	separator -height 10;

	// paste to locked
	setParent ..;
	rowLayout	-numberOfColumns 4  -cat 1 "right" 5 -cat 3 "both" 5 -columnWidth 1 95 -columnWidth 2 30 -columnWidth 3 155 -parent $col;
		text -l "Paste to locked";
		checkBox	-h 20	-value 0 -label "" rig_PasteLockedCB;

	// paste to selected objects only
		text -l "Paste to selected objects only";
		checkBox	-h 20	-value 0 -label ""
				-onc "checkBox -e -ed 0 -v 0 rig_MultiPasteCB;"
				-ofc "checkBox -e -ed 1 rig_MultiPasteCB;"
				 rig_PasteSelectedCB;

	setParent ..;
	separator -height 10;

	// paste to Multiple objects
	setParent ..;
	rowLayout	-numberOfColumns 2  -cat 1 "right" 5 -columnWidth 1 95  -parent $col;
		text -l "MULTI-PASTE";
		checkBox	-h 20	-value 0 -label "   (i.e.  Paste to multiple characters)"
				-onc "checkBox -e -ed 0 -v 0 rig_PasteSelectedCB; checkBox -e -ed 0 -v 0 cbPrefixReplace;"
				-ofc "checkBox -e -ed 1      rig_PasteSelectedCB; checkBox -e -ed 1      cbPrefixReplace;"
				 rig_MultiPasteCB;

	// paste channels button
	string $pasteButton = `button	-l "Paste Channels" -parent $form`;
	button -e -c ("pasteAnim(\"" + $filename + "\"); deleteUI rig_LoadSelAnimWindow;") $pasteButton;

	formLayout	-edit
				-af $frame "top"	3 
				-af $frame "left"	3 
				-af $frame "right"	3 
				-ac $frame "bottom"	3 $pasteButton 

				-an $pasteButton "top"
				-af $pasteButton "left" 3
				-af $pasteButton "right" 3
				-af $pasteButton "bottom" 3
				$form;

	showWindow rig_LoadSelAnimWindow;

} // loadAnimGUI


//------------------------------------------------------------------------------------------- 
// pasteAnim
//
// Description:  
//
// Inputs:  string $filename    - the filename to read from
//
// Returns: none
//
//------------------------------------------------------------------------------------------- 
global proc pasteAnim (string $filename) {

	global string $rig_SelObjList[];

	string $attrList[];
	string $nameBuf[];
	string $objList[];
	string $filePath[];
	string $obj;
	string $atr;
	string $oldPre  = `textField -q -text rig_TextOldPre`;
	string $newPre  = `textField -q -text rig_TextNewPre`;
	string $oldText = `textField -q -text rig_TextOld`;
	string $newText = `textField -q -text rig_TextNew`;

	int    $gotIt;
	int 	  $numKeys;
	int    $pasteMethod   = `radioButtonGrp -q -sl rig_PasteMethBG`; 
	int    $replacePrefix = `checkBox -q -v cbPrefixReplace`;
	int    $searchReplace = `checkBox -q -v cbSearchReplace`;
	int    $pasteLocked   = `checkBox -q -v rig_PasteLockedCB`;
	int    $pasteSelected = `checkBox -q -v rig_PasteSelectedCB`;
	int    $multiPaste    = `checkBox -q -v rig_MultiPasteCB`;
	int    $startFrame    = `intField  -q -v rig_StartFrameIF`;
	int    $endFrame      = `intField  -q -v rig_EndFrameIF`;
	int    $offset        = `intField  -q -v rig_OffsetIF`;
					
	// if we're multiPasting, do it in another proc
	if ($multiPaste) {
		print ("Calling multiPasteAnim\n");
		rig_MultiPasteAnim($filename);
		return;
	}

	// print some useful info: 
	print ("Loading animation from file: " + $filename + "\n" );

	if ($replacePrefix) {

		print ("Replacing prefixes. \n");
		print ("	Old Prefix: " + $oldPre + "\n" );
		print ("	New Prefix: " + $newPre + "\n" );
	}

	if ($searchReplace) print ("Replacing \"" + $oldText + "\" with \"" + $newText + "\"\n");

	if ($pasteLocked) print "Pasting to locked channels..\n\n";

	print ("Paste Method = " + $pasteMethod + "\n");

	if ($pasteMethod != 3) {

		print ("\tStart frame: " + $startFrame + "\n");
		print ("\tEnd frame:   " + $endFrame + "\n");
		print ("\tOffset:      " + $offset + "\n\n");
	}

	tokenize($filename, "/", $filePath);
	string $buf = $filePath[size($filePath)-1];
	string $fileObj = substituteAllString($buf, "_", "");

	// if we're pasting to selected only, make sure something's selected
	if ($pasteSelected) {

		print ("Pasting to selected objects only\n");

		if (size($rig_SelObjList) < 1) {
			if (( `window -ex rig_LoadSelAnimWindow`) == true) deleteUI rig_LoadSelAnimWindow;
    			delete ("TEMP" + $fileObj);
			error("rig_PasteSelAnim: paste to selected objects checked but nothing selected");
		}
	}
    string $TEMP_NODE = "__ANIM_TEMP_" + $fileObj;
	$attrList = `listAttr $TEMP_NODE`;

	for ($attrCur in $attrList) {
		// Copy keys 
		if (match("ANIM", $attrCur) != "") {       

			print ("Working on " + $attrCur + "\n");

			string $tempString = substitute ("ANIM", $attrCur, "^");
			tokenize( $tempString, "^", $nameBuf); 
			$obj = $nameBuf[0];
			$atr = $nameBuf[1];
			print $obj;
			print $atr;
			if (`gmatch $obj "*__*"`){
                //string $objNS = `substitute "__" $obj ":"`;
                string $objNS = `substitute "__" $obj ""`;
                $obj = $objNS ;
            }

			$numKeys = `keyframe -query -keyframeCount ($TEMP_NODE + "." + $attrCur)`;
			print ("Found " + $numKeys + " keyframe(s) on " + $obj + "." + $atr +  "\n");

			if ($numKeys > 0) {

				//if ($replacePrefix) $obj = `substitute $oldPre $obj $newPre`;
				if ($replacePrefix){
				    if (`gmatch $oldPre ""`){
                        $obj = ($newPre + $obj);
                        print $obj;
                    }
                    else{
                        $obj = `substitute $oldPre $obj $newPre`;
                    }
				}
				if ($searchReplace) $obj = `substitute $oldText $obj $newText`; 

				// check if the obj we're pasting to exists
				if (!`objExists($obj)`) {
					print ("WARNING: Couldn't find: " + $obj + " to paste to.  Skipping\n");
					continue;
				}

				// check if the channel exists on the object we're pasting to
				if (!`attributeExists $atr $obj`) {
					print ("WARNING: Couldn't find: " + $atr + " on " + $obj + " to paste to.  Skipping\n");
					continue;
				}

				// if we're pasting to selected objects only, check if the obj has been selected
				if ($pasteSelected) {
					$gotIt = false;
					for ($sel in $rig_SelObjList) {
						if ($obj == $sel) {
							$gotIt = true;
							break;
						}
					}

					if (!$gotIt) {
						print ("WARNING: can't find " + $obj + " in selection list.  Skipping\n");
						continue;
					}
				}

				$numKeys = `copyKey -t ":" -at ($attrCur) $TEMP_NODE`;
				string $numExpr[] = `listConnections -s true -d false -t "expression"  -scn true ($obj + "." + $atr)`;

				if (size($numExpr) != 0) {

					print $numExpr;
					print ("WARNING: attribute has expression defined.  Ignoring animation\n");
					continute;
				}

				// check if the target attribute is locked
				if (`getAttr -l ($obj + "." + $atr) `)	{

					if ($pasteLocked) {

						print ("\t\tUNLOCKING: " + $obj + "." + $atr + "\n");
						setAttr -lock false   ($obj + "." + $atr);
						setAttr -keyable true ($obj + "." + $atr);

					} else { 

						print ("\t\tLOCKED ATTRIBUTE: " + $obj + "." + $atr + "   Skipping\n");
						continue;
					}
				} 

				switch ($pasteMethod) {

					case 1: // insert

						catch($numKeys =  `pasteKey	 -t $startFrame -option insert -at $atr $obj`);
						break;

					case 2: // replace

						catch($numKeys =  `pasteKey	 -t (($startFrame + $offset) + ":" + ($endFrame + $offset))
												 -option replace -at $atr $obj`);
						break;

					case 3: // replace completely

						catch($numKeys =  `pasteKey  -option replaceCompletely -at $atr $obj`);
						break;

				}

				if ($numKeys) 
					print ("Animation curve pasted to " + $obj + "." + $atr +  "\n");
				else
					print ("WARNING: nothing pasted to " + $obj + "." + $atr +  "\n");
                 
			} // if numKeys
		
		else if ($numKeys == 0) 
			{
			
			if (!`attributeExists $atr $obj`) 
				{
					print ("WARNING: Couldn't find: " + $atr + " on " + $obj + " to paste to.  Skipping\n");
					continue;
				}
			else
				{
				$staticValue = `getAttr ("TEMP" + $fileObj + "." + $attrCur)` ;
				catch(`setAttr ($obj + "." + $atr) $staticValue`) ;
				print ("Static value " + $staticValue + " pasted to " + $obj + "." + $atr +  "\n");
				}
			}
		}

	}

    //delete ("TEMP" + $fileObj);
    delete $TEMP_NODE;
}  // pasteAnim 


global proc multiPasteAnim (string $filename) {

	global string $rig_SelObjList[];

	string $attrList[];
	string $nameBuf[];
	string $tokens[];
	string $objList[];
	string $filePath[];
	string $obj;
	string $atr;
	string $oldPre = "";
	string $newPre = "";
	string $oldText = `textField -q -text rig_TextOld`;
	string $newText = `textField -q -text rig_TextNew`;

	int 	  $numKeys;
	int    $pasteMethod   = `radioButtonGrp -q -sl rig_PasteMethBG`; 
	int    $searchReplace = `checkBox -q -v cbSearchReplace`;
	int    $pasteLocked   = `checkBox -q -v rig_PasteLockedCB`;
	int    $startFrame    = `intField  -q -v rig_StartFrameIF`;
	int    $endFrame      = `intField  -q -v rig_EndFrameIF`;
	int    $offset        = `intField  -q -v rig_OffsetIF`;
					

	// print some useful info: 
	print ("Loading animation from file: " + $filename + "\n" );

	if ($searchReplace) print ("Replacing \"" + $oldText + "\" with \"" + $newText + "\"\n");

	if ($pasteLocked) print "Pasting to locked channels..\n\n";

	print ("Paste Method = " + $pasteMethod + "\n");

	if ($pasteMethod != 3) {

		print ("\tStart frame: " + $startFrame + "\n");
		print ("\tEnd frame:   " + $endFrame + "\n");
		print ("\tOffset:      " + $offset + "\n\n");
	}

	tokenize($filename, "/", $filePath);
	string $buf = $filePath[size($filePath)-1];
	string $fileObj = substituteAllString($buf, "_", "");

	if (size($rig_SelObjList) < 1) {
		if (( `window -ex rig_LoadSelAnimWindow`) == true) deleteUI rig_LoadSelAnimWindow;
    		delete ("TEMP" + $fileObj);
		error("multiPasteAnim: nothing selected to paste to");
	}
 
	$attrList = `listAttr ("TEMP" + $fileObj)`;

	// loop through the attributes until we find a saved
	// animation channel then extract the object name
	for ($attrCur in $attrList) {

		if (match("ANIM", $attrCur) != "") {       

			string $tempString = substitute ("ANIM", $attrCur, "^");
			tokenize( $tempString, "^", $nameBuf); 
			break;
		}
	}

	// extract the prefix
	$numTokens = `tokenize $nameBuf[0] "_" $tokens`;

	for ($obj in $rig_SelObjList) {

		$numTokens = `tokenize $obj "_" $nameBuf`;

		for ($attrCur in $attrList) {

			// Copy keys 
			if (match("ANIM", $attrCur) != "") {       

				print ("Working on " + $attrCur + "\n");

				string $tempString = substitute ("ANIM", $attrCur, "^");
				tokenize( $tempString, "^", $nameBuf); 
				$obj = $nameBuf[0];
				$atr = $nameBuf[1];

				$numKeys = `keyframe -query -keyframeCount ("TEMP" + $fileObj + "." + $attrCur)`;
				print ("Found " + $numKeys + " keyframe(s) on " + $obj + "." + $atr +  "\n");

				if ($numKeys > 0) {

					$obj = `substitute $oldPre $obj $newPre`; 
					if ($searchReplace) $obj = `substitute $oldText $obj $newText`; 

					// check if the obj we're pasting to exists
					if (!`objExists($obj)`) {
						print ("WARNING: Couldn't find: " + $obj + " to paste to.  Skipping\n");
						continue;
					}

					// check if the channel exists on the object we're pasting to
					if (!`attributeExists $atr $obj`) {
						print ("WARNING: Couldn't find: " + $atr + " on " + $obj + " to paste to.  Skipping\n");
						continue;
					}

					$numKeys = `copyKey -t ":" -at ($attrCur) ("TEMP" + $fileObj)`;
					string $numExpr[] = `listConnections -s true -d false -t "expression"  -scn true ($obj + "." + $atr)`;

					if (size($numExpr) != 0) {
	
						print $numExpr;
						print ("WARNING: attribute has expression defined.  Ignoring animation\n");
						continute;
					}
	
					// check if the target attribute is locked
					if (`getAttr -l ($obj + "." + $atr) `)	{

						if ($pasteLocked) {

							print ("\t\tUNLOCKING: " + $obj + "." + $atr + "\n");
							setAttr -lock false   ($obj + "." + $atr);
							setAttr -keyable true ($obj + "." + $atr);

						} else { 
	
							print ("\t\tLOCKED ATTRIBUTE: " + $obj + "." + $atr + "   Skipping\n");
							continue;
						}
					} 
	
					switch ($pasteMethod) {
	
						case 1: // insert
	
							catch($numKeys =  `pasteKey	 -t $startFrame -option insert -at $atr $obj`);
							break;
	
						case 2: // replace
	
							catch($numKeys =  `pasteKey	 -t (($startFrame + $offset) + ":" + ($endFrame + $offset))
													 -option replace -at $atr $obj`);
							break;
	
						case 3: // replace completely
	
							catch($numKeys =  `pasteKey  -option replaceCompletely -at $atr $obj`);
							break;
					}
	
					if ($numKeys) 
						print ("Animation curve pasted to " + $obj + "." + $atr +  "\n");
					else
						print ("WARNING: nothing pasted to " + $obj + "." + $atr +  "\n");
	
				}
			}
		}
	}

    delete ("TEMP" + $fileObj);

}  // multiPasteAnim 
