import logging
LOG = logging.getLogger(__name__)
import os
import maya.cmds as cmds


def FBXbrowserWindow(filter_="FBX", caption_="IMPORT FBX ANIM FILE", fileMode_="file"):
    multipleFilters = None
    filtersOld = None

    if filter_ == "FBX":
        multipleFilters = "*.fbx"
    else:
        multipleFilters = ""

    window = None
    if fileMode_ == "directory":
        window = cmds.fileDialog2(fileFilter=multipleFilters, dialogStyle=2, caption=caption_, fileMode=3, okCaption="Select")
    else:
        window = cmds.fileDialog2(fileFilter="*.fbx", dialogStyle=2, caption=caption_, fileMode=1, okCaption="Import")

    if window:
        return window[0]
    else:
        return window


def importFBXNamespace_callback(fbx_file):

    sel = cmds.ls(selection=True)
    if sel:
        namespace = sel[0].split(":")
        if namespace:
            ns = namespace[0]
            importFBXFileToNamespace(ns, fbx_file)
            LOG.info("Imported FBX anim file {} onto namespace {}".format(fbx_file, ns))
        else:
            LOG.info("Selected does not have namespace, use standard FBX importer")
    else:
        LOG.info("Nothing selected, please select one node on namespaced rig and try again!")



def importFBXFileToNamespace(namespaceStr, fbxFile):
    """Use to import fbx file/anim onto namespace targets

    Args:
        namespaceStr - Namespace of target node receiving animation
        fbxFile - Full path to fbx file to be imported.

    Example:
        # Importing non-namespaced FBX joint anim file into scene
        # with namespaced nodes
        PROJ_PATH = cmds.workspace(q=True, rd=True)
        FBX_FILE = PROJ_PATH+"fbx/joint_test_anim.fbx"

        importFBXFileToNamespace("test", FBX_FILE)
    """
    if os.path.isfile(fbxFile):
        LOG.info("FBX file exists: {}".format(fbxFile))

        # Import fbx file as is
        fbxNodes = cmds.file(fbxFile, i=True, type="FBX", rnn=True)

        # Sort through imported fbx nodes, copy anim from tfms/jnts to target nodes
        for fbxNode in fbxNodes:
            if cmds.objectType(fbxNode) == "joint" or cmds.objectType(fbxNode) == "transform":
                fbxNode = fbxNode.split("|")[-1]
                channels = cmds.listAttr(fbxNode, keyable=True)

                for ch in channels:
                    srcAnimCrv = cmds.listConnections("{}.{}".format(fbxNode, ch), s=True, d=False)

                    if srcAnimCrv:
                        if cmds.objExists("{}:{}".format(namespaceStr, fbxNode)):
                            # Delete any existing curves on target nodes first
                            tgtAnimCrv = cmds.listConnections("{}:{}.{}".format(namespaceStr, fbxNode, ch), s=True,
                                                            d=False)
                            if tgtAnimCrv:  cmds.delete(tgtAnimCrv)

                            # Duplicate srcAnimCrv and rename to namespace
                            tgtAnimCrv = cmds.duplicate(srcAnimCrv[0])[0]
                            tgtAnimCrv = cmds.rename(tgtAnimCrv, "{}:{}".format(namespaceStr, srcAnimCrv[0]))

                            # Connect duplicated src curve and connect into target tfm/jnt
                            cmds.connectAttr("{}.output".format(tgtAnimCrv), "{}:{}.{}".format(namespaceStr, fbxNode, ch),
                                           force=True)
                            LOG.debug("Connected {} to {}".format(srcAnimCrv[0], "{}.{}".format(fbxNode, ch)))

        # Delete imported fbx nodes
        cmds.delete(fbxNodes)

        return True

    else:
        LOG.info("{} does not exist!".format(fbxFile))
        return