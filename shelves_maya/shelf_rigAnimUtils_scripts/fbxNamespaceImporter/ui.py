"""

import fbxNamespaceImporter
reload(fbxNamespaceImporter)
fbxNamespaceImporter.show()

"""
import logging
LOG = logging.getLogger(__name__)

from Qt import QtWidgets, QtCore, QtGui

try:
    from shiboken2 import wrapInstance
    import shiboken2 as shiboken
    import pyside2uic  as pysideuic

except ImportError:
    from shiboken import wrapInstance
    import shiboken
    import pysideuic

import os
import sys

from cStringIO import StringIO
import xml.etree.ElementTree as xml

import maya.cmds as cmds
import maya.OpenMayaUI as openMayaUI
import maya.mel as mel

from fbxNamespaceImporter import core
reload(core)


def show():
    global importExportWindow
    try:
        importExportWindow.close()
    except:
        pass

    importExportWindow = fbxNamespaceImporterUI()
    importExportWindow.show()
    return importExportWindow

def loadUiType(uiFile):
    """
    Pyside lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
    and then execute it in a special frame to retrieve the form_class.
    http://tech-artists.org/forum/showthread.php?3035-PySide-in-Maya-2013
    """
    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text

    with open(uiFile, 'r') as f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec pyc in frame

        #Fetch the base_class and form class based on their type in the xml from designer
        form_class = frame['Ui_%s'%form_class]
        base_class = eval('QtWidgets.%s' % widget_class)
	return form_class, base_class

def getMayaWindow():
    ptr = openMayaUI.MQtUtil.mainWindow()
    if ptr is not None:
		return wrapInstance(long(ptr), QtWidgets.QWidget)

uiFile = None	
try:
    selfDirectory = os.path.dirname(__file__)
    uiFile = selfDirectory + '/fbxNamespaceImporter.ui'
    print "Opening {}".format(uiFile)
except:
	raise Exception

if os.path.isfile(uiFile):
    form_class, base_class = loadUiType(uiFile)
else:
    cmds.error('Cannot find UI file: ' + uiFile)


class fbxNamespaceImporterUI(base_class, form_class):
    title = 'FBX Namespace Anim Import 1.0'

    def __init__(self, parent=getMayaWindow()):
        self.closeExistingWindow()
        super(fbxNamespaceImporterUI, self).__init__(parent)

        self.setupUi(self)
        self.setWindowTitle(self.title)

        self.ieFolderButton.clicked.connect(self.browseFn)
        self.ieImportButton.clicked.connect(self.importFn)

        wName = openMayaUI.MQtUtil.fullName(long(shiboken.getCppPointer(self)[0]))
        self.refreshUI()
		
    def closeExistingWindow(self):
        for qt in QtWidgets.QApplication.topLevelWidgets():
            try:
                if qt.__class__.__name__ == self.__class__.__name__:
                    qt.deleteLater()
                    print ('fbxNamespaceImporterUI: Closed ' + str(qt.__class__.__name__))

            except:
                pass

    def browseFn(self):
        fbx_file = core.FBXbrowserWindow()
        if fbx_file:
            self.ieFilePathLabel.setText(fbx_file)

    def importFn(self):

        fbx_file = self.ieFilePathLabel.text()
        if os.path.isfile(fbx_file):
            core.importFBXNamespace_callback(fbx_file)
        else:
            LOG.warning("File does not exist: {}".format(fbx_file))


    ###############
    ## REFRESH UI
    ###############
    def refreshUI(self):

        print 'refreshUI completed.'

if __name__ == '__main__':
    show()