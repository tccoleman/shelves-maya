"""

# Cache geometry
1. Open animation shot
2. Export geometry to Alembic cache
    - Set time range
    - Write UV sets
    - Strip namespace

# Bake and export face wrinkle animation
3. Select the face mesh
4. On the RigAnim_utils shelf, select Textures > Bake and export face wrinkle shader animation...
5. Select a directory and filename for the face wrinkle animation file

# Open master render file for character
6. Open your master render scene, not rig, just geo and lights
7. Select geo and import Alembic cache
8. Import baked face wrinkle animation
9. Test render


"""


import os
import tempfile
import maya.cmds as cmds
import maya.mel as mel

import logging
LOG = logging.getLogger(__name__)

SCRIPTS_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), ''))

def get_namespace(node_name):
    """Return namespace of node_name (if it exists)"""
    if cmds.objExists(node_name):
        if ':' in node_name:
            return node_name.split(':')[0]
        else:
            return


def get_shader(node_name):
    """Return assigned shader of node_name"""
    if cmds.objExists(node_name):
        # Check if we have a mesh
        shp = cmds.listRelatives(node_name, shapes=True)
        if shp:
            if "mesh" in cmds.nodeType(shp[0]):
                # Get attached shaders from mesh
                cmds.select(shp[0])
                mel.eval('hyperShade -smn "";')
                shaders = cmds.ls(selection=True)
                if shaders:
                    return shaders
            else:
                print "Selected object is not a mesh: {}".format(node_name)


def get_head_material(shader_list, regex="_head_"):
    """Returns the head material (if any) assigned to node_name"""
    if shader_list:
        head_mat = None
        for shader in shader_list:
            if regex in shader:
                head_mat = shader
        return head_mat


def get_mask_nodes(node_name):
    """Returns mask file nodes of node_name"""
    if cmds.objExists(node_name):
        ns = get_namespace(node_name)
        shdrs = get_shader(node_name)
        head_mat = get_head_material(shdrs)
        if ns:
            mask_nodes = cmds.ls("{}:Mask*".format(ns), type="file")
        else:
            mask_nodes = cmds.ls("Mask*".format(ns), type="file")
        if mask_nodes:
            return mask_nodes


def get_current_range():
    """Get the time range in the current Maya scene.

    Returns:
        list: Frames for start and end
    """
    return [cmds.playbackOptions(q=True, min=True), cmds.playbackOptions(q=True, max=True)]


def bake_mask_nodes(node_name):
    """Bakes expression animation on colorGain attrs on all mask nodes"""
    rgb_attrs = ["colorGainR", "colorGainG", "colorGainB"]
    mask_nodes = get_mask_nodes(node_name)
    start_end = get_current_range()
    cmds.bakeResults(mask_nodes, at=rgb_attrs, t=(start_end[0], start_end[1]), sm=True, sb=1, dic=True, pok=False)
    # cmds.bakeResults(['yanchixia:Mask_1_2_3','yanchixia:Mask_4_5_6'], at=['colorGainR','colorGainG','colorGainB'], t=(1, 10), sm=True, sb=1, dic=True, pok=False)


def export_shader(shader):
    """Bring up browser to export shader to"""
    print "Exporting shader: {}".format(shader)


def launch_anim_import_export(mode="export"):
    ANIMIO_DIR = SCRIPTS_DIR.replace('\\', '/')
    mel.eval('source "{}//animImportExport.mel"'.format(ANIMIO_DIR))

    if "import" in mode:
        mel.eval('animFileLoad();')

    elif "export" in mode:
        mel.eval('animFileSave();')

    else:
        pass


def export_baked_face_shader():
    """Bakes face material attrs and exports """
    # Check selection
    sel = cmds.ls(selection=True)

    if sel:
        node_name = sel[0]
        ns = get_namespace(node_name)

        # Get head shader
        shdrs = get_shader(node_name)
        if shdrs:
            head_shdr = get_head_material(shdrs)
            LOG.info("Head mesh: {}, Shader: {}".format(node_name, head_shdr))
            if head_shdr:
                # Confirm bake
                result = cmds.confirmDialog(title='Confirm', message='Bake face shader {}?'.format(head_shdr),
                                             button=['Yes', 'No'], defaultButton='Yes', cancelButton='No',
                                             dismissString='No')
                if result:
                    PROJ_PATH = cmds.workspace(q=True, rd=True)
                    TEMP_DIR = tempfile.gettempdir()
                    CUR_FILE_PATH = cmds.file(q=True, sn=True)
                    CUR_FILE = cmds.file(q=True, sn=True, shn=True)

                    # Save temp file
                    LOG.info("Saving temp file...")
                    cmds.file(rename='%s\\%s' % (TEMP_DIR, CUR_FILE))
                    cmds.file(save=True, type='mayaAscii')

                    # Import reference
                    LOG.info("Importing referenced file...")
                    if isReferenced(node_name):
                        refNode = getReferenceNode(node_name)
                        if refNode:
                            importReference(refNode, verbose=True)

                    # Remove namespace
                    LOG.info("Removing namespace...")
                    nmSpace = node_name.rpartition(':')
                    if nmSpace:
                        cmds.namespace(removeNamespace=nmSpace[0], mergeNamespaceWithParent=True)
                        node_name = node_name.replace("{}:".format(ns), "")

                    # Do baking
                    LOG.info("Baking {} shader...".format(head_shdr))
                    # Hide viewport
                    modelPanels = cmds.getPanel(vis=True)
                    emptySelConn = cmds.selectionConnection()

                    for panel in modelPanels:
                        if cmds.getPanel(to=panel) == 'modelPanel':
                            cmds.isolateSelect(panel, state=True)
                            cmds.modelEditor(panel, e=True, mlc=emptySelConn)
                    print "node_name = {}".format(node_name)
                    bake_mask_nodes(node_name)

                    # Show viewport
                    cmds.deleteUI(emptySelConn)
                    for panel in modelPanels:
                        if cmds.getPanel(to=panel) == 'modelPanel':
                            cmds.isolateSelect(panel, state=False)

                    mask_nodes = get_mask_nodes(node_name)
                    cmds.select(mask_nodes)
                    LOG.info("Save baked face wrinkle animation file...".format(head_shdr))
                    launch_anim_import_export()

                    cmds.select(node_name)

                    LOG.info("Successfully saved baked face wrinkle animation file")

                    # Try to reopen original file and delete temp file
                    try:
                        cmds.file(CUR_FILE_PATH, o=True, pmt=False, f=True)
                        LOG.info("Successfully reopened original file")
                        os.remove('{}\\{}'.format(TEMP_DIR, CUR_FILE))
                        LOG.info("Successfully removed temp file")
                    except:
                        LOG.error("Unable to open original file: {}".format(CUR_FILE_PATH))
                        return
                else:
                    LOG.warning("Bake face wrinkle shader cancelled")
                    return
            else:
                LOG.error("Cannot find head material shader for {}".format(node_name))
    else:
        LOG.error("Nothing selected, select character head mesh and try again...")

def listReferences(parentNS=None):
    """Return a list of reference nodes found in the current scene
    @param
    Args:
        parentNS: Parent namespace to query reference nodes from
    """
    # Get Reference List
    refNodes = []
    for ref in cmds.ls(type='reference'):
        if 'sharedReferenceNode' in ref: continue
        if '_UNKNOWN_REF_NODE_' in ref: continue
        if parentNS:
            if not ref.startswith(parentNS):
                continue
        refNodes.append(ref)

    return refNodes

def isReference(refNode):
    '''
    Query if the given node is a valid reference node
    @param refNode: Reference node to query
    @type refNode: str
    '''
    # Check reference node
    if refNode not in listReferences(): return False

    # Return Result
    return True


def isReferenced(node):
    '''
    Query if the given node is referenced from an external file
    @param node: Reference node to query
    @type node: str
    '''
    # Check referenced node
    if cmds.referenceQuery(node, isNodeReferenced=True): return True
    else: return False


def getReferenceNode(node):
    '''
    Get the reference node associated with the given referenced object
    @param refNode: Reference node to query
    @type refNode: str
    '''
    # Check Referenced Node
    if not isReferenced(node):
        raise Exception('Object "' + node + '" is not a referenced node!')

    # Get Reference Node
    refNode = cmds.referenceQuery(node, referenceNode=True)

    # Return Result
    return refNode


def importReference(refNode, verbose=True):
    '''
    Import the reference associated with the given reference node
    @param refNode: Reference node to import
    @type refNode: str
    @param verbose: Print detailed progress messages
    @type verbose: bool
    '''
    # Check reference node
    if not isReference(refNode):
        raise Exception('Object "' + refNode + '" is not a valid reference node!')

    # Get referenced file path
    refFile = ''
    try:
        # Get reference file path
        refFile = cmds.referenceQuery(refNode, filename=True)

    except:
        # Check reference node
        if cmds.objExists(refNode):
            # Print message
            if verbose: print('No file associated with reference! Deleting node "' + refNode + '"')

            # Delete refNode
            cmds.lockNode(refNode, l=False)
            cmds.delete(refNode)

    else:
        # Import reference
        cmds.file(refFile, importReference=True)

        # Print message
        if verbose: print('Imported reference "' + refNode + '" from - "' + refFile)