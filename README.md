## Introduction

Python module with functions to aid in creating/editing custom Maya shelves.

"shelf_rigAnimUtils.py" is an example of a custom Maya shelf created using the shelf_base 
class.

## Installation
1) Download the shelves-maya module

2) Unzip the downloaded zip file of shelves-maya.

3) Inside the unzipped shelves-maya folder there will be another shelves-maya folder.  Put this shelves-maya directory 
wherever you put your other Maya scripts.

4) See "Usage" below for two options for running shelves-maya

## Usage
Using Maya Script Editor:
```
import sys
# Substitute the path below to your installation path
sys.path.append('D:\Users\Tcoleman\Documents\dev\shelves-maya')

from shelves_maya import shelf_rigAnimUtils
shelf_rigAnimUtils.load(name="RigAnim_utils")
```

Using userSetup.py to load shelf at Maya startup every time (add code below to your userSetup.py)
```
import maya.utils
    
# Custom python paths
sys.path.append('D://Users//Tcoleman//Documents//dev//shelves-maya')
    
# Custom Shelves
from shelves_maya import shelf_rigAnimUtils
    
def load_custom_shelves():
    shelf_rigAnimUtils.load(name="RigAnim_utils")
    print "Loaded Custom Maya Shelves..."
    
maya.utils.executeDeferred("load_custom_shelves()")
```
You can add more custom shelves to load in the "load_custom_shelves" function above.


## Author
Tim Coleman - tim.coleman.3d@gmail.com

Original shelf_base class, idea and credit from Vasil Shotarov:
http://bindpose.com/scripting-custom-shelf-in-maya-python/