import os
import io
from setuptools import setup, find_packages


NAME = 'shelves-maya'
DESCRIPTION = 'Maya shelf tools'
AUTHOR = 'Tim Coleman'
EMAIL = 'tcoleman@magicleap.com'
URL = 'https://github.magicleap.com/tcoleman/shelves-maya'
KEYWORDS = ['maya', 'shelf']


here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'colors', '__version__.py')) as f:
    exec(f.read(), about)


setup(name=NAME,
      version=about['__version__'],
      description=DESCRIPTION,
      long_description=DESCRIPTION,
      author=AUTHOR,
      author_email=EMAIL,
      url=URL,
      install_requires=REQUIRED,
      keywords=KEYWORDS,
      packages=find_packages(exclude=('tests',))
      )


